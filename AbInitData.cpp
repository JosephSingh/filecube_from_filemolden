#include"AbInitData.h"

AbInitData::AbInitData(const char* fname) : geom(0),basis(0),xyzBasis(0),Orbitals(0) {
    Orbitals.clear();
    ifstream fin(fname);
    if(!loadGeom(&fin) || !loadBasis(&fin) || !loadOrbitals(&fin)) {
        throw string("File canonot be read. Check that you put .molden file as input of the programm");
    }
    fin.close();
}
// ���������
bool AbInitData::loadGeom(istream* fin){// only ang
    string inp;
    if(!fin->good()) {
        cout << "Error! File is bad\n";
        return false;
    }
    while(inp != "[Atoms] Angs") getline(*fin, inp);
    if(fin->eof()) {
        cout << "Error! Reached end of file without finding geometry (IT MUST BE IN ANG)\n";
        return false;
    }

    int nAtoms=0;
    getline(*fin, inp);
    while(inp != "[GTO]" && !fin->eof()) {
        stringstream ss(inp);
        GeomAtom A;
        ss >> A.label >> A.number_in_file >> A.n_elec;
        ss >> A.x >> A.y >> A.z;
        A.x*=ANG_TO_BOHR;
        A.y*=ANG_TO_BOHR;
        A.z*=ANG_TO_BOHR;
        cout << "Atom " << A.label << " has been added\n";
        geom.push_back(A);
        nAtoms++;
        getline(*fin, inp);
    }
    cout << nAtoms << " atoms\n";
    if(nAtoms == 0) return false;
    if(fin->eof() ) return false;

    return true;
}
bool AbInitData::loadBasis(istream* fin){
    cout << "Loading basis - start\n";

    vector<OneBF> atomBasis;
    atomBasis.clear();
    OneBF oneBF;
    string inp, atomName;

    enum MODES { newAtom, newBF, readBF}; // � ����� ��� ��� ��� ���� ������� ��� ������ �������� �� ������������ ��������
    MODES modes = newAtom;

    while (inp != "[MO]" && !fin->eof()) {
        getline(*fin, inp);
        vector<string> parsed = parseWrds(inp);

        if ((modes==newAtom)&&(parsed.size()==2)) {
            atomName = geom[stoi(parsed[0])-1].label;
            cout << "Starting to read basis for " << atomName << endl;
            modes=newBF;
            cout << inp << endl;
            continue;
        }

        if ((modes==readBF)&&(parsed.size()==0)){ // ��������� � ������ ������ ������ �����
            cout << "BF reading finished, atom basis complete\n";
            atomBasis.push_back(oneBF);
            basis.push_back(make_pair(atomName, atomBasis));
            atomBasis.clear();
            oneBF.clear();
            modes=newAtom;
            cout << inp << endl;
            continue;
        }

        if ((modes==readBF)&&(parsed.size()==3)){ // ��������� ������ �������� ������� (��� �������� ��� �����������, �������� �� ������������!)
            atomBasis.push_back(oneBF);
            oneBF.clear();
            cout << "BF reading finished\n";
            modes=newBF;
        }
        if ((modes==newBF)&&(parsed.size()==3)){ // ��������� � ������ ����� �������� �������
            cout << "New BF\n";
            cout << inp << endl;
            oneBF.clear();
            oneBF.shell = OneBF::determineShell(parsed[0]);
            if (oneBF.shell == OneBF::undef) {
                cout<<"Error! Type shell \""<<parsed[0]<< " is not recognized !\n";
                return false;
            }
            modes = readBF;
            continue;
        }
        if ((modes==readBF)&&(parsed.size()==2)){ // ���������� ������ �������� �������
            double alpha = myStod(parsed[0]);
            double C = myStod(parsed[1]);
            if(abs(C) > BASIS_COEFF_PRECISION) {
                oneBF.add(alpha,C);
                cout << inp << endl;
            }
            continue;
        }
        cout << "Line has been skipped\n";
    }
    if (basis.size()!=geom.size()){
        cout<<"Error! Number of basis centers("<<basis.size()<<") is not equal number of geometry centers ("<<geom.size()<<")!\n";
        return false;
    }
    // It'a alrady normalised
    setupXyzBasis();
    return true;
}
bool AbInitData::loadOrbitals(istream* fin){
    cout << "Loading orbitals\n";
    string tStr;
    vector<double> candidates; candidates.clear();
    candidates.reserve(xyzBasis.size());
    double Occ(0);

    enum MODES {newOrb, readOrb};

    getline(*fin,tStr);
    if(fin->eof()) return false;
    MODES mode = newOrb;

    while (getline(*fin,tStr)) {
        if(mode == newOrb) {
            cout << "Reading new orbital from .molden\t";
            getline(*fin,tStr);
            getline(*fin,tStr);
            vector<string> parsed = parseWrds(tStr);
            cout << tStr << endl;
            Occ = stod(parsed[1]); // Occupation numbers are not in scientific notation
            mode = readOrb;
            continue;
        }
        if(mode == readOrb) {
            vector<string> parsed = parseWrds(tStr);
            try {stoi(parsed[0]);}
            catch(const std::invalid_argument& ia) {
                if (candidates.size() != xyzBasis.size()){
                    cout<<"Error! Number of xyz primitives in basis ("<<xyzBasis.size()<<") is not equal number of orbitals coefficients ("<<candidates.size()<<")!\n";
                    return false;
                }
                Orbital A(candidates, Occ);
                Orbitals.push_back(A);
                candidates.clear();
                mode = newOrb;
                continue;
            }
            try{
                candidates.push_back(myStod(parsed[1]));
            }
            catch(const std::invalid_argument& ia) {
                cout << parsed[1] << " -------------!!!!!! Some orbital coefficient in .molden is bad. Orbital skipped, occupation number = 0.0 assigned !!!!!!-----------------\n";
                candidates.push_back(0.0);
                Occ = 0.0;
            }
            continue;
        }
        cout << "File ended, read " << Orbitals.size() << " orbitals\n";
    }
    if (Orbitals.size() == 0) {
        cout << "Error! No orbitals have been found in current file\n";
        return false;
    }
    else return true;
}

void AbInitData::setupXyzBasis(){
    DeCartFunc deCartFunc;
    xyzBasis.clear();
    cout << "Atoms:" << basis.size() << endl;
    for (size_t i=0; i<basis.size(); i++) {
        cout << "BF's for " << basis[i].first << ": " << basis[i].second.size() << endl;
        for (size_t j=0; j<basis[i].second.size(); j++){// Order if defined by .molden fomat specification: http://cheminf.cmbi.ru.nl/molden/molden_format.html
            if (basis[i].second[j].shell == OneBF::S ){
                deCartFunc.set(i,j,0,0,0); xyzBasis.push_back(deCartFunc);
            }
            if (basis[i].second[j].shell == OneBF::P ){
                deCartFunc.set(i,j,1,0,0); xyzBasis.push_back(deCartFunc);
                deCartFunc.set(i,j,0,1,0); xyzBasis.push_back(deCartFunc);
                deCartFunc.set(i,j,0,0,1); xyzBasis.push_back(deCartFunc);
            }
            if (basis[i].second[j].shell == OneBF::D ){
                deCartFunc.set(i,j,2,0,0); xyzBasis.push_back(deCartFunc);
                deCartFunc.set(i,j,0,2,0); xyzBasis.push_back(deCartFunc);
                deCartFunc.set(i,j,0,0,2); xyzBasis.push_back(deCartFunc);
                deCartFunc.set(i,j,1,1,0); xyzBasis.push_back(deCartFunc);
                deCartFunc.set(i,j,1,0,1); xyzBasis.push_back(deCartFunc);
                deCartFunc.set(i,j,0,1,1); xyzBasis.push_back(deCartFunc);
            }
            if (basis[i].second[j].shell == OneBF::F ){
                deCartFunc.set(i,j,3,0,0); xyzBasis.push_back(deCartFunc);
                deCartFunc.set(i,j,0,3,0); xyzBasis.push_back(deCartFunc);
                deCartFunc.set(i,j,0,0,3); xyzBasis.push_back(deCartFunc);
                deCartFunc.set(i,j,1,2,0); xyzBasis.push_back(deCartFunc);
                deCartFunc.set(i,j,2,1,0); xyzBasis.push_back(deCartFunc);
                deCartFunc.set(i,j,2,0,1); xyzBasis.push_back(deCartFunc);
                deCartFunc.set(i,j,1,0,2); xyzBasis.push_back(deCartFunc);
                deCartFunc.set(i,j,0,1,2); xyzBasis.push_back(deCartFunc);
                deCartFunc.set(i,j,0,2,1); xyzBasis.push_back(deCartFunc);
                deCartFunc.set(i,j,1,1,1); xyzBasis.push_back(deCartFunc);
            }
            if (basis[i].second[j].shell == OneBF::G ){
                deCartFunc.set(i,j,4,0,0); xyzBasis.push_back(deCartFunc);
                deCartFunc.set(i,j,0,4,0); xyzBasis.push_back(deCartFunc);
                deCartFunc.set(i,j,0,0,4); xyzBasis.push_back(deCartFunc);
                deCartFunc.set(i,j,3,1,0); xyzBasis.push_back(deCartFunc);
                deCartFunc.set(i,j,3,0,1); xyzBasis.push_back(deCartFunc);
                deCartFunc.set(i,j,1,3,0); xyzBasis.push_back(deCartFunc);
                deCartFunc.set(i,j,0,3,1); xyzBasis.push_back(deCartFunc);
                deCartFunc.set(i,j,1,0,3); xyzBasis.push_back(deCartFunc);
                deCartFunc.set(i,j,0,1,3); xyzBasis.push_back(deCartFunc);
                deCartFunc.set(i,j,2,2,0); xyzBasis.push_back(deCartFunc);
                deCartFunc.set(i,j,2,0,2); xyzBasis.push_back(deCartFunc);
                deCartFunc.set(i,j,0,2,2); xyzBasis.push_back(deCartFunc);
                deCartFunc.set(i,j,2,1,1); xyzBasis.push_back(deCartFunc);
                deCartFunc.set(i,j,1,2,1); xyzBasis.push_back(deCartFunc);
                deCartFunc.set(i,j,1,1,2); xyzBasis.push_back(deCartFunc);
            }
        }
    }
}

double AbInitData::getOrbitalValue(const size_t OrbN, const double& x, const double& y, const double& z) const{
    double res(0);
    for (size_t i=0; i<xyzBasis.size(); i++){
        if(abs(Orbitals[OrbN].C[i]) > ORBITAL_COEFF_PRECISION) {
            double xC = geom[xyzBasis[i].idXYZ].x;
            double yC = geom[xyzBasis[i].idXYZ].y;
            double zC = geom[xyzBasis[i].idXYZ].z;
            double dx=x-xC;
            double dy=y-yC;
            double dz=z-zC;
            double r2=dx*dx+dy*dy+dz*dz;
            int nx = xyzBasis[i].nx;
            int ny = xyzBasis[i].ny;
            int nz = xyzBasis[i].nz;
            vector<pair<double, double> > aC=basis[xyzBasis[i].idXYZ].second[xyzBasis[i].idAlphaC].alpha_C;
            double partRes(0);
            for (size_t j=0; j<aC.size(); j++){
                double S=overlap_I1_3D(xC,yC,zC,aC[j].first,nx,ny,nz,
                                       xC,yC,zC,aC[j].first,nx,ny,nz);
                partRes +=exp(-aC[j].first*r2)/sqrt(S)*aC[j].second;
            }
            res+=partRes*(Orbitals[OrbN].C[i])*pow(dx,nx)*pow(dy,ny)*pow(dz,nz);
        }
    }
    return res;
}

bool AbInitData::calcMassCenter(double& xC, double& yC, double& zC, const char* filename) const {
    xC = yC = zC = 0;
    if (geom.size() == 0) {
        cout << "Error! Isn't possible to calculate mass center coordinates without loaded geometry!\n";
        return false;
    }

    double* mass = new double[geom.size()];
    ifstream inp(filename);
    if(!inp.good()) {
        cout << "Error! File " << filename << " cannot be read\n";
        return false;
    }

    for(auto atom : geom) {
        int atomic_number = 0;
        mass[atom.number_in_file-1] = 0;
        while (atomic_number != atom.n_elec) {
            inp >> atomic_number >> mass[atom.number_in_file-1];
            mass[atom.number_in_file-1]*=mTOm;
            if(inp.eof()) {
                cout << "Error! No data about " << atom.label << " mass in file " << filename << endl;
                return false;
            }
        }
        cout<<"   For element "<<atom.label<<" mass "<<mass[atom.number_in_file-1]<< " a.u. was detemined\n";
        inp.seekg(0);
    }
    inp.close();

    double m(0);
    for (auto atom : geom) {
        if(mass[atom.number_in_file-1] < mTOm) {
            cout << "Error! No data about " << atom.label << " mass in file " << filename << endl;
            return false;
        }
        m  += mass[atom.number_in_file-1];
        xC += mass[atom.number_in_file-1] * atom.x;
        yC += mass[atom.number_in_file-1] * atom.y;
        zC += mass[atom.number_in_file-1] * atom.z;
    }
    xC /= m;
    yC /= m;
    zC /= m;
    cout<<"   Coordinates for center mass (Bohr) are: "<<xC<<" "<<yC<<" "<<zC<< endl;
    delete[] mass;
    return true;
}
