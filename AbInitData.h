#ifndef ABINITDATA_H_INCLUDED
#define ABINITDATA_H_INCLUDED

#include"Basic_geom_orbitals.h"
#include<stdexcept>

#define mTOm 1822.888677
#define ORBITAL_OCC_PRECISION   0.0000005
#define ORBITAL_COEFF_PRECISION 0.000000000005
#define BASIS_COEFF_PRECISION   0.00000000005
#define GEOMETRY_PRECISION      0.00000000005

class AbInitData {
public:
    vector<GeomAtom>                        geom;
	vector<pair<string,vector<OneBF> > >    basis; //   �������� ������� �� ������ �����
	vector<DeCartFunc>                      xyzBasis;// ������� ����� � ���������� ����
	vector<Orbital>                         Orbitals;

	AbInitData(const char* fname);
   ~AbInitData(){}

	bool loadGeom(istream* fin);// ��������� �� ���� ������ � ����������
	bool loadBasis(istream* fin);
	bool loadOrbitals(istream* fin);

	void setupXyzBasis();//	������������ ����������� ������. � ���������� ������, ����� �������� ������� �� ������� .molden
    double getOrbitalValue(const size_t OrbN, const double& x, const double& y, const double& z) const;
    bool calcMassCenter(double& xC, double& yC, double& zC, const char* filename) const;
private:
    AbInitData() = delete;
};

#endif // ABINITDATA_H_INCLUDED
