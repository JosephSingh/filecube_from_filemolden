#ifndef BASIC_GEOM_ORBITALS_H_INCLUDED
#define BASIC_GEOM_ORBITALS_H_INCLUDED

#include<fstream>
#include<cstring>
#include<sstream>
#include<vector>
#include<iomanip>
#include<ctime>

#include"calcOverlap.cpp"

#define ANG_TO_BOHR 1.889725989

using namespace std;

class Timer {
    clock_t tStart;
public:
    Timer() : tStart(clock()) {}
    double currTime() const {return double(clock() - tStart);}
    void restart() {tStart = clock();}
    void print() const {cout << "time: " << currTime()/CLOCKS_PER_SEC <<" s\n";}
};

vector<string> parseWrds(const string& str){
	vector<string> res;
	res.clear();
	string tStr;
	stringstream inp(str);
	while (inp>>tStr) res.push_back(tStr);
	return res;
}

double myStod(string& num) { //Fortran Scientific Notation Be Damned
    size_t FSNBD = num.find_first_of("Dd");
    if(FSNBD != string::npos) {
        num[FSNBD] = 'E';
    }
    return stod(num);
}

class GeomAtom {
public:
    //enum Atoms {H("H", "He", "Li", "Be", "B", "C", "N", "O", "F", "Ne"};
    double x, y, z;
    int n_elec;
    int number_in_file;
	string label;

	friend ostream& operator<<(ostream& fout, const GeomAtom& A);

	GeomAtom(): x(0), y(0), z(0), n_elec(0), number_in_file(0), label("") {}
};

ostream& operator<<(ostream& fout, const GeomAtom& A) {
    fout<<setw(5)<<fixed<<A.n_elec<<setw(12)<<setprecision(6)<<(double)A.n_elec
         <<setw(12)<<setprecision(6)<<A.x
         <<setw(12)<<setprecision(6)<<A.y
         <<setw(12)<<setprecision(6)<<A.z;
    return fout;
}

struct Vector3D {
    double x,y,z;
    Vector3D(const double inX, const double inY, const double inZ) : x(inX),y(inY),z(inZ) {}

    friend ostream& operator<<(ostream& fout, const Vector3D& A);
    double operator*(const Vector3D& B) const {return x*B.x+y*B.y+z*B.z;}
};

ostream& operator<<(ostream& fout, const Vector3D& A) {
    fout<<setw(12)<<setprecision(6)<<A.x<<setw(12)<<A.y<<setw(12)<<A.z;
    return fout;
}

class DeCartFunc {
public:
	DeCartFunc() : idXYZ(-1), idAlphaC(-1), nx(-1), ny(-1), nz(-1) {}
	void set (const int& idXYZ_, const int& idAlphaC_, const int& nx_, const int& ny_, const int& nz_) {
		idXYZ=idXYZ_;
		idAlphaC=idAlphaC_;
		nx=nx_;
		ny=ny_;
		nz=nz_;
	}
	int idXYZ, idAlphaC, nx, ny, nz;
};

class OneBF {
public:
    vector<pair<double, double> > alpha_C;
	enum SHELL { undef, S, P, D, F, G};
	SHELL shell;
	static SHELL determineShell(const string& str){
		if (str=="S" || str=="s") return S;
		if (str=="P" || str=="p") return P;
		if (str=="D" || str=="d") return D;
		if (str=="F" || str=="f") return F;
		if (str=="G" || str=="g") return G;
		return undef;
	}
	OneBF(): alpha_C(0), shell(undef) {alpha_C.clear();}
	void clear(){ shell = undef; alpha_C.clear(); }
	void add(const double& alpha, const double& C){ alpha_C.push_back(make_pair(alpha,C) ); }
};

class Orbital {
public:
    vector<double> C;
    double Occup;

    Orbital() : C(0),Occup(0) {}
    Orbital(vector<double>& inpC, const double& inpOcc) : C(inpC), Occup(inpOcc) {}
};

#endif // BASIC_GEOM_ORBITALS_H_INCLUDED
