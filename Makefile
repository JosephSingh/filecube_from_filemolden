CXX = g++
CXXFLAGS = -std=c++11 -Wall -pedantic -Wextra -Weffc++ -O2 
MOL ?= lih_avqz

.PHONY: test

compile: main.cpp
	$(CXX) $(CXXFLAGS) main.cpp -o MakeCube 

test: compile
	./MakeCube lih_avqz.molden 64 64 64 > lih_test.log
	diff lih_avqz_64x64x64.cube lih_test_ver_64
	diff lih_test.log lih_test_log_ver

cube: compile
	./MakeCube $(MOL).molden > $(MOL).log &

check:
	dos2unix *.molden
	cppcheck --enable=all --inconclusive main.cpp

clean: 
	rm *.cube *.log MakeCube

