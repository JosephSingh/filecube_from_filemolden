#include"PackedBasis.h"

void PackedXyzBasis::clear(){
    x=y=z=alpha=0;
    for (int i=0; i<CARTESIAN_BASIS_SPDFG; i++) c[i]=0;
    isS = isP = isD = isF = isG = false;
    PreCalcXYZ = nullptr;
}

void PackedXyzBasis::set(const double& x_, const double& y_, const double& z_, const double& a_){ // TODO �����-������ ��� ������ ���� �� ���������� ��������
    x=x_; y=y_; z=z_; alpha=a_;
}

double PackedXyzBasis::getPolynomeValue(const double& valueX, const double& valueY, const double& valueZ) const{
    const double dx = valueX - x;
    const double dy = valueY - y;
    const double dz = valueZ - z;

    const double dx2 = dx * dx;
    const double dy2 = dy * dy;
    const double dz2 = dz * dz;

    double res(0);
    if (isS) res += c[0];
    if (isP) res += c[1]*dx  + c[2]*dy  + c[3]*dz;
    if (isD) res += c[4]*dx2 + c[5]*dy2 + c[6]*dz2 + c[7]*dx*dy + c[8]*dx*dz + c[9]*dy*dz;
    if (isF) res += ( c[10]*dx+c[13]*dy+c[14]*dz )*dx2 + ( c[11]*dy+c[15]*dx+c[16]*dz )*dy2 + ( c[12]*dz+c[17]*dx+c[18]*dy )*dz2 + c[19]*dx*dy*dz;
    if (isG) {
        res += dx2*( dx*(c[20]*dx + c[23]*dy + c[24]*dz) + c[29]*dy2 + c[30]*dz2 + c[32]*dy*dz)
            +  dy2*( dy*(c[21]*dy + c[25]*dx + c[26]*dz) + c[31]*dz2 + c[33]*dx*dz)
            +  dz2*( dz*(c[22]*dz + c[27]*dx + c[28]*dy) + c[34]*dx*dy);
    }
    return res;
}

double PackedXyzBasis::getPolynomeValue() const{
    double res(0);
    if(isS) {res += c[0];}
    if(isP) {
        for(int i=1; i<4; i++) {
            res += c[i]*PreCalcXYZ[i];
        }
    }
    if(isD) {
        for(int i=4; i<10; i++) {
            res += c[i]*PreCalcXYZ[i];
        }
    }
    if(isF) {
        for(int i=10; i<20; i++) {
            res += c[i]*PreCalcXYZ[i];
        }
    }
    if(isG) {
        for(int i=20; i<CARTESIAN_BASIS_SPDFG; i++) {
            res += c[i]*PreCalcXYZ[i];
        }
    }
    return res;
}

bool PackedXyzBasis::addCbyNxNyNz(const int& nx, const int& ny, const int& nz, const double& C){
    if ( (nx+ny+nz) == 0) isS = true;
    if ( (nx+ny+nz) == 1) isP = true;
    if ( (nx+ny+nz) == 2) isD = true;
    if ( (nx+ny+nz) == 3) isF = true;
    if ( (nx+ny+nz) == 4) isG = true;
    if ( (nx==0)&&(ny==0)&&(nz==0) ) { c[ 0] += C; return true; }
    if ( (nx==1)&&(ny==0)&&(nz==0) ) { c[ 1] += C; return true; }
    if ( (nx==0)&&(ny==1)&&(nz==0) ) { c[ 2] += C; return true; }
    if ( (nx==0)&&(ny==0)&&(nz==1) ) { c[ 3] += C; return true; }
    if ( (nx==2)&&(ny==0)&&(nz==0) ) { c[ 4] += C; return true; }
    if ( (nx==0)&&(ny==2)&&(nz==0) ) { c[ 5] += C; return true; }
    if ( (nx==0)&&(ny==0)&&(nz==2) ) { c[ 6] += C; return true; }
    if ( (nx==1)&&(ny==1)&&(nz==0) ) { c[ 7] += C; return true; }
    if ( (nx==1)&&(ny==0)&&(nz==1) ) { c[ 8] += C; return true; }
    if ( (nx==0)&&(ny==1)&&(nz==1) ) { c[ 9] += C; return true; }

    if ( (nx==3)&&(ny==0)&&(nz==0) ) { c[10] += C; return true; }
    if ( (nx==0)&&(ny==3)&&(nz==0) ) { c[11] += C; return true; }
    if ( (nx==0)&&(ny==0)&&(nz==3) ) { c[12] += C; return true; }
    if ( (nx==2)&&(ny==1)&&(nz==0) ) { c[13] += C; return true; }
    if ( (nx==2)&&(ny==0)&&(nz==1) ) { c[14] += C; return true; }
    if ( (nx==1)&&(ny==2)&&(nz==0) ) { c[15] += C; return true; }
    if ( (nx==0)&&(ny==2)&&(nz==1) ) { c[16] += C; return true; }
    if ( (nx==1)&&(ny==0)&&(nz==2) ) { c[17] += C; return true; }
    if ( (nx==0)&&(ny==1)&&(nz==2) ) { c[18] += C; return true; }
    if ( (nx==1)&&(ny==1)&&(nz==1) ) { c[19] += C; return true; }

    if ( (nx==4)&&(ny==0)&&(nz==0) ) { c[20] += C; return true; }
    if ( (nx==0)&&(ny==4)&&(nz==0) ) { c[21] += C; return true; }
    if ( (nx==0)&&(ny==0)&&(nz==4) ) { c[22] += C; return true; }
    if ( (nx==3)&&(ny==1)&&(nz==0) ) { c[23] += C; return true; }
    if ( (nx==3)&&(ny==0)&&(nz==1) ) { c[24] += C; return true; }
    if ( (nx==1)&&(ny==3)&&(nz==0) ) { c[25] += C; return true; }
    if ( (nx==0)&&(ny==3)&&(nz==1) ) { c[26] += C; return true; }
    if ( (nx==1)&&(ny==0)&&(nz==3) ) { c[27] += C; return true; }
    if ( (nx==0)&&(ny==1)&&(nz==3) ) { c[28] += C; return true; }
    if ( (nx==2)&&(ny==2)&&(nz==0) ) { c[29] += C; return true; }
    if ( (nx==2)&&(ny==0)&&(nz==2) ) { c[30] += C; return true; }
    if ( (nx==0)&&(ny==2)&&(nz==2) ) { c[31] += C; return true; }
    if ( (nx==2)&&(ny==1)&&(nz==1) ) { c[32] += C; return true; }
    if ( (nx==1)&&(ny==2)&&(nz==1) ) { c[33] += C; return true; }
    if ( (nx==1)&&(ny==1)&&(nz==2) ) { c[34] += C; return true; }
    return false;

}

