#ifndef PACKEDBASIS_H_INCLUDED
#define PACKEDBASIS_H_INCLUDED

#define CARTESIAN_BASIS_SPDFG 35

class PackedXyzBasis {
public:
	PackedXyzBasis(): x(0),y(0),z(0),alpha(0),c{0},isS(false),isP(false),isD(false),isF(false),isG(false),PreCalcXYZ(nullptr){clear();}

	double x,y,z,alpha,c[CARTESIAN_BASIS_SPDFG];//  0  s;  1   x;  2  y;   3   z;   4 x2;  5  y2;  6  z2;  7  xy;  8  xz;  9  yz
	bool isS,isP,isD,isF,isG;                   // 10  x3; 11  y3; 12 z3;  13 x2y; 14 x2z; 15 xy2; 16 y2z; 17 xz2; 18 yz2; 19 xyz
                                                // 20  x4; 21  y4; 22 z4;  23 x3y; 24 x3z; 25 xy3; 26 y3z; 27 xz3; 28 yz3; 30x2y2;
    double* PreCalcXYZ;                         // 30 x2z2 31 y2z2 32 x2yz 33 xy2z 34 zyz2

	void clear();
	void set(const double& x_, const double& y_, const double& z_, const double& a_); // TODO �����-������ ��� ������ ���� �� ���������� ��������

	double getPolynomeValue(const double& valueX, const double& valueY, const double& valueZ) const;
	double getPolynomeValue() const;

	void getXyzAlpha(double& x_, double& y_, double& z_, double& a_) const  { x_ = x; y_ = y; z_ = z; a_ = alpha; }
	bool addCbyNxNyNz(const int& nx, const int& ny, const int& nz, const double& C);
	bool isContain(const double& x_, const double& y_, const double& z_, const double& alpha_) const {
	    bool xEq = abs(x-x_)<GEOMETRY_PRECISION;
	    bool yEq = abs(y-y_)<GEOMETRY_PRECISION;
	    bool zEq = abs(z-z_)<GEOMETRY_PRECISION;
	    bool aEq = abs(alpha_-alpha)<BASIS_COEFF_PRECISION;
	    return (xEq && yEq && zEq && aEq);
    }

    PackedXyzBasis& operator*=(const double& factor) {
        for(size_t t=0; t<CARTESIAN_BASIS_SPDFG; t++) c[t] *= factor;
        return *this;
    }
    bool operator==(const PackedXyzBasis& B) const {
        if(abs(x-B.x)<GEOMETRY_PRECISION && abs(y-B.y)<GEOMETRY_PRECISION && abs(z-B.z)<GEOMETRY_PRECISION && abs(alpha-B.alpha)<BASIS_COEFF_PRECISION){
            if((isS == B.isS)&&(isP == B.isP)&&(isD == B.isD)&&(isF == B.isF)&&(isG == B.isG))return true;
            else return false;
        }
        else return false;
    }
};

#endif // PACKEDBASIS_H_INCLUDED
