
class Vec3D {
public:
    double x,y,z;
    double slen;

    double len() const {
        return sqrt(slen);
    }

    Vec3D():x(0),y(0),z(0),slen(0) {}
    Vec3D(const double& inx, const double& iny, const double& inz):x(inx),y(iny),z(inz),slen(inx*inx+iny*iny+inz*inz) {}
    //Basic operations
    Vec3D operator+(const Vec3D& B) const {
        Vec3D C(x+B.x, y+B.y, z+B.z);
        return C;
    }
    Vec3D operator*(const double& f) const {
        Vec3D B(x*f,y*f,z*f);
        return B;
    }
    double operator*(const Vec3D& B) const {
        return x*B.x + y*B.y + z*B.z;
    }
    Vec3D operator/(const double& f) const {
        Vec3D B(x/f,y/f,z/f);
        return B;
    }
    Vec3D& operator*=(const double& f) {
        x*=f; y*=f; z*=f; slen*=f*f;
        return *this;
    }
    Vec3D& operator/=(const double& f) {
        x/=f; y/=f; z/=f; slen*=f*f;
        return *this;
    }
};

ostream& operator<<(ostream& fout, const Vec3D& B) {
    fout<<fixed<<setprecision(6)<<setw(12)<<B.x<<setw(12)<<B.y<<setw(12)<<B.z;
    return fout;
}

istream& operator>>(istream& fin, Vec3D& B) {
    fin>>B.x>>B.y>>B.z;
    return fin;
}
