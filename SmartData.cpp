#include"SmartData.h"

bool SmartOrbital::setDeCartFromFF(const AbInitData& ini, const size_t OrbN, double** const PreCalcXYZ){
    if(ini.Orbitals[OrbN].Occup < ORBITAL_OCC_PRECISION) return false; // unoccupied = no reason to load

    xyzBasis.clear();
    for (size_t i=0; i<ini.xyzBasis.size(); i++){
        const size_t CenteredOn = ini.xyzBasis[i].idXYZ;

        const double x = ini.geom[CenteredOn].x;
        const double y = ini.geom[CenteredOn].y;
        const double z = ini.geom[CenteredOn].z;
        const int nx = ini.xyzBasis[i].nx;
        const int ny = ini.xyzBasis[i].ny;
        const int nz = ini.xyzBasis[i].nz;
        for (size_t j=0; j<ini.basis[CenteredOn].second[ini.xyzBasis[i].idAlphaC].alpha_C.size(); j++){
            const double alpha=ini.basis[CenteredOn].second[ini.xyzBasis[i].idAlphaC].alpha_C[j].first;
            const double S=overlap_I1_3D(x,y,z,alpha,nx,ny,nz,
                                         x,y,z,alpha,nx,ny,nz);
            const double C=ini.basis[CenteredOn].second[ini.xyzBasis[i].idAlphaC].alpha_C[j].second*(ini.Orbitals[OrbN].C[i])/sqrt(S);
            int pos(-1);
            for (size_t k=0; k<xyzBasis.size(); k++) {
                if (xyzBasis[k].isContain(x,y,z,alpha)) pos = k;
            }
            if (pos == -1) {
                PackedXyzBasis tBasis;
                tBasis.PreCalcXYZ = PreCalcXYZ[CenteredOn];
                tBasis.set(x,y,z,alpha);
                tBasis.addCbyNxNyNz(nx,ny,nz,C);
                xyzBasis.push_back(tBasis);
            } else {
                xyzBasis[pos].addCbyNxNyNz(nx,ny,nz,C);
            }
        }
    }
    for (auto& func : xyzBasis) {
        func *= sqrt(ini.Orbitals[OrbN].Occup);
    }
    return true;
}

//-Smart data 1---------------------------------------------------------------------------------

SmartData::SmartData(const AbInitData& ini) : PreCalcXYZ(nullptr),geom(ini.geom),ExpBasis(0),NewOrbitals(0) {
    NewOrbitals.clear();
    size_t nTotalFunc(0);
    for(size_t i=0; i<ini.xyzBasis.size(); i++) nTotalFunc += ini.basis[ini.xyzBasis[i].idXYZ].second[ini.xyzBasis[i].idAlphaC].alpha_C.size();
    cout <<"   Initial FF orbitals contain "<<nTotalFunc<<" functions grouped into "<<ini.xyzBasis.size()<<" groups.\n";

    PreCalcXYZ = new double*[geom.size()];
    for(size_t a=0; a<geom.size(); a++) {
        PreCalcXYZ[a] = new double[CARTESIAN_BASIS_SPDFG];
    }

    for(size_t t=0; t<ini.Orbitals.size(); t++) {
        SmartOrbital A;
        if(A.setDeCartFromFF(ini, t, PreCalcXYZ)) NewOrbitals.push_back(A);
    }

    ExpBasis.clear();
    for(auto& bas_func : NewOrbitals[0].xyzBasis) {
        ExpBasis.emplace_back(bas_func);
    }
    cout << "   New ExpBasis created, it has " << ExpBasis.size() << " different exponents\n";
    cout << "   This molecule has " << NewOrbitals.size() << " natural MO's\n";
}

double ExpTIme = 0;
double MulTime = 0;
double XyzTime = 0;

inline void Fill_CXYZ(double* XYZ, const double& x, const double& y, const double& z) {
    XYZ[0] = 1;//  0  s;
    //cout << "p\n";
    XYZ[1] = x;//  1   x;  2  y;   3   z;
    XYZ[2] = y;
    XYZ[3] = z;
    //cout << "d\n";
    XYZ[4] = x*x;// 4 x2;  5  y2;  6  z2;  7  xy;  8  xz;  9  yz
    XYZ[5] = y*y;
    XYZ[6] = z*z;
    XYZ[7] = x*y;
    XYZ[8] = x*z;
    XYZ[9] = y*z;
    //cout << "f\n";
    XYZ[10] = XYZ[4]*x; // 10  x3; 11  y3; 12 z3;  13 x2y; 14 x2z; 15 xy2; 16 y2z; 17 xz2; 18 yz2; 19 xyz
    XYZ[11] = XYZ[5]*y;
    XYZ[12] = XYZ[6]*z;
    XYZ[13] = XYZ[4]*y;
    XYZ[14] = XYZ[4]*z;
    XYZ[15] = x*XYZ[5];
    XYZ[16] = XYZ[5]*z;
    XYZ[17] = x*XYZ[6];
    XYZ[18] = y*XYZ[6];
    XYZ[19] = XYZ[7]*z;
    //cout << "g\n";
    XYZ[20] = XYZ[4]*XYZ[4];// 20  x4; 21  y4; 22 z4;  23 x3y; 24 x3z; 25 xy3; 26 y3z; 27 xz3; 28 yz3; 30x2y2;
    XYZ[21] = XYZ[5]*XYZ[5];
    XYZ[22] = XYZ[6]*XYZ[6];
    XYZ[23] = XYZ[10]*y;
    XYZ[24] = XYZ[10]*z;
    XYZ[25] = x*XYZ[11];
    XYZ[26] = XYZ[11]*z;
    XYZ[27] = x*XYZ[12];
    XYZ[28] = y*XYZ[12];
    XYZ[29] = XYZ[4]*XYZ[5];
    XYZ[30] = XYZ[4]*XYZ[6]; //30 x2z2 31 y2z2 32 x2yz 33 xy2z 34 zyz2
    XYZ[31] = XYZ[5]*XYZ[6];
    XYZ[32] = XYZ[4]*XYZ[9];
    XYZ[33] = XYZ[5]*XYZ[8];
    XYZ[34] = XYZ[7]*XYZ[6];
    //cout << "done\n";
}

double SmartData::getDensityValue(const double& x, const double& y, const double& z) const {
    Timer T;
    vector<double> Exponents; Exponents.clear();
    Exponents.reserve(ExpBasis.size());
    for(auto& alpha : ExpBasis) {
        Exponents.push_back(alpha.getValue(x,y,z));
    }
    ExpTIme += T.currTime();
    T.restart();

    for(auto& At : geom) {
        Fill_CXYZ(PreCalcXYZ[At.number_in_file-1], x-At.x, y-At.y, z-At.z);
    }
    XyzTime += T.currTime();
    T.restart();

    double res = 0;
    for(auto& orb : NewOrbitals) {
        double curr_res = 0;
        for(size_t i=0; i<orb.xyzBasis.size(); i++) {
            curr_res += Exponents[i]*orb.xyzBasis[i].getPolynomeValue();
        }
        res += curr_res*curr_res;
    }
    MulTime += T.currTime();
    T.restart();
    return res;
}

double SmartData::getDensityValue(const double& x, const double& y, const double& z, double* Exp) const {
    Timer T;
    for(auto& At : geom) {
        Fill_CXYZ(PreCalcXYZ[At.number_in_file-1], x-At.x, y-At.y, z-At.z);
    }
    XyzTime += T.currTime();
    T.restart();

    double res = 0;
    for(auto& orb : NewOrbitals) {
        double curr_res = 0;
        for(size_t i=0; i<orb.xyzBasis.size(); i++) {
            curr_res += Exp[i]*orb.xyzBasis[i].getPolynomeValue();
        }
        res += curr_res*curr_res;
    }
    MulTime += T.currTime();
    T.restart();
    return res;
}

void SmartData::PrintBothArbitraryCubes(const string& comment,
               const double& xOrig, const double& yOrig, const double& zOrig,
               const Vec3D& e1, const Vec3D& e2, const Vec3D& e3,
               const size_t& Dim1, const size_t& Dim2, const size_t& Dim3,
               const size_t& CtrDim1, const size_t& CtrDim2, const size_t& CtrDim3,
               const char* f_uncontr_name, const char* f_contr_name)
               const {
    //--------------------------------------------------------------------------------------------------------
    if(Dim1 % CtrDim1 != 0 || Dim3 % CtrDim3 != 0 || Dim2 % CtrDim2 != 0) throw string("Can't contract cube with these parameters");
    const size_t C1(Dim1/CtrDim1), C2(Dim2/CtrDim2), C3(Dim3/CtrDim3);//Dimensions of small cube making one value of the contracted
    ofstream classic(f_uncontr_name);
    ofstream contracted(f_contr_name);
    Vec3D Orig(xOrig, yOrig, zOrig);
    Vec3D CtrOrig(Orig + (e1*(C1/2)) + (e2*(C2/2)) + (e3*(C3/2)) );
    // Head
    contracted << comment << endl;
    contracted << "Cube_maker v0.5b\n";// Cube origin is moved because of contraction by C1/2*xUnit an is right at the center of the first small C1*C2*C3 cube

    contracted <<setw(5)<<fixed <<geom.size()<<CtrOrig<<endl;
    contracted <<setw(5)<<fixed <<CtrDim1<<e1*C1<<endl;
    contracted <<setw(5)<<fixed <<CtrDim2<<e2*C2<<endl;
    contracted <<setw(5)<<fixed <<CtrDim3<<e3*C3<<endl;

    classic << comment << endl;
    classic << "Cube_maker v0.5b\n";
    classic <<setw(5)<<fixed <<geom.size() <<setw(12)<<setprecision(6)<<Orig<<endl;
    classic <<setw(5)<<fixed <<Dim1<<e1<<endl;
    classic <<setw(5)<<fixed <<Dim2<<e2<<endl;
    classic <<setw(5)<<fixed <<Dim3<<e3<<endl;
    // Geometry
    for (auto atom : geom) {
        contracted << atom << endl;
        classic    << atom << endl;
    }
    // Body
    Timer T;
    double Cache[CtrDim2][CtrDim3];//Caching array of small cubes because order is wrong. We must complete the whole "plate" i.e. cover all yz plane
                                  //before printing values to the contracted cube
    for(size_t j2=0; j2<CtrDim2; j2++) {
        for(size_t j3=0; j3<CtrDim3; j3++) {
            Cache[j2][j3] = 0;
        }
    }
    cout << "Working with array = " << T.currTime() << endl;

    for(size_t i1=0; i1<Dim1; i1++) {// Each step is one small cube (if we contract 256^3 to 64^3 CtrDim1=CtrDim1=CtrDim3=64 and first three C2cles
    for(size_t i2=0; i2<Dim2; i2++) {// skip 4 values by each step
    for(size_t i3=0; i3<Dim3; i3++) {
        double x = xOrig + i1*e1.x + i2*e2.x + i3*e3.x;
        double y = yOrig + i1*e1.y + i2*e2.y + i3*e3.y;
        double z = zOrig + i1*e1.z + i2*e2.z + i3*e3.z;
        //--------------All magic happens here------------------
        double res = getDensityValue(x,y,z);
        size_t j2 = (i2-(i2%C2))/C2; // Number of contracted cube point which values are currently being calculated
        size_t j3 = (i3-(i3%C3))/C3;
        Cache[j2][j3] += res;
        //------------------------------------------------------
        classic<<setw(16)<<std::scientific<<setprecision(8)<<res;
        if (i3 % 6 == 5) classic<<endl;
        }//End of outer z loop
        classic<<endl;
        }//End of outer y loop
        cout << "Printing stored values to the contracted cube\n";
        if(i1%C1 == C1-1) {
            for(size_t p2=0; p2<CtrDim2; p2++) {
            for(size_t p3=0; p3<CtrDim3; p3++) {
                contracted<<setw(16)<<std::scientific<<setprecision(8)<<Cache[p2][p3];
                Cache[p2][p3] = 0;
                if (p3 % 6 == 5) contracted<<endl;
            }
            contracted << endl;
            }
        }
    }//End of outer x loop
    contracted.close();
    classic.close();
    cout << "ExpTime = " << ExpTIme/CLOCKS_PER_SEC << endl;
    cout << "mulTime = " << MulTime/CLOCKS_PER_SEC << endl;
    cout << "xyzTime = " << XyzTime/CLOCKS_PER_SEC << endl;
}

void SmartData::PrintBothOrthogonalCubes(const string& comment,
               const double& xOrig, const double& yOrig, const double& zOrig,
               const Vec3D& e1, const Vec3D& e2, const Vec3D& e3,
               const size_t& Dim1, const size_t& Dim2, const size_t& Dim3,
               const size_t& CtrDim1, const size_t& CtrDim2, const size_t& CtrDim3,
               const char* f_uncontr_name, const char* f_contr_name)
               const {
    //--------------------------------------------------------------------------------------------------------
    if(Dim1 % CtrDim1 != 0 || Dim3 % CtrDim3 != 0 || Dim2 % CtrDim2 != 0) throw string("Can't contract cube with these parameters");
    if(e1*e2 || e2*e3 || e1*e3) throw string("Vectors are not orthogonal");

    const size_t C1(Dim1/CtrDim1), C2(Dim2/CtrDim2), C3(Dim3/CtrDim3);//Dimensions of small cube making one value of the contracted
    ofstream classic(f_uncontr_name);
    ofstream contracted(f_contr_name);
    Vec3D Orig(xOrig, yOrig, zOrig);
    Vec3D CtrOrig(Orig + (e1*(C1/2)) + (e2*(C2/2)) + (e3*(C3/2)));
    // Head
    contracted << comment << endl;
    contracted << "Cube_maker v0.5o\n";// Cube origin is moved because of contraction by C1/2*xUnit an is right at the center of the first small C1*C2*C3 cube

    contracted <<setw(5)<<fixed <<geom.size()<<CtrOrig<<endl;
    contracted <<setw(5)<<fixed <<CtrDim1<<e1*C1<<endl;
    contracted <<setw(5)<<fixed <<CtrDim2<<e2*C2<<endl;
    contracted <<setw(5)<<fixed <<CtrDim3<<e3*C3<<endl;

    classic << comment << endl;
    classic << "Cube_maker v0.5o\n";
    classic <<setw(5)<<fixed <<geom.size() <<setw(12)<<setprecision(6)<<Orig<<endl;
    classic <<setw(5)<<fixed <<Dim1<<e1<<endl;
    classic <<setw(5)<<fixed <<Dim2<<e2<<endl;
    classic <<setw(5)<<fixed <<Dim3<<e3<<endl;
    // Geometry
    for (auto atom : geom) {
        contracted << atom << endl;
        classic    << atom << endl;
    }
    // Body
    double Cache[CtrDim2][CtrDim3];//Caching array of small cubes because order is wrong. We must complete the whole "plate" i.e. cover all yz plane
                                  //before printing values to the contracted cube
    for(size_t j2=0; j2<CtrDim2; j2++) {
        for(size_t j3=0; j3<CtrDim3; j3++) {
            Cache[j2][j3] = 0;
        }
    }

    double Exp1[ExpBasis.size()];
    double Exp2[ExpBasis.size()][Dim2];
    double Exp3[ExpBasis.size()][Dim3];

    double* Exponents = new double[ExpBasis.size()];

    Timer T;
    for(size_t i1=0; i1<Dim1; i1++) {
        T.restart();
        for(size_t t=0; t<ExpBasis.size(); t++) {
            Exp1[t]     = ExpBasis[t].getValueOrt(i1, e1, Orig);
        }
        ExpTIme += T.currTime();
        for(size_t i2=0; i2<Dim2; i2++) {
            if(i1==0) {
                T.restart();
                for(size_t t=0; t<ExpBasis.size(); t++) {
                    Exp2[t][i2] = ExpBasis[t].getValueOrt(i2, e2, Orig);
                }
                ExpTIme += T.currTime();
            }
            for(size_t i3=0; i3<Dim3; i3++) {
                if(i1==0 & i2==0) {
                    T.restart();
                    for(size_t t=0; t<ExpBasis.size(); t++) {
                        Exp3[t][i3] = ExpBasis[t].getValueOrt(i3, e3, Orig);
                    }
                    ExpTIme += T.currTime();
                }
                double x = xOrig + i1*e1.x + i2*e2.x + i3*e3.x;
                double y = yOrig + i1*e1.y + i2*e2.y + i3*e3.y;
                double z = zOrig + i1*e1.z + i2*e2.z + i3*e3.z;
                //--------------All magic happens here------------------
                for(size_t t=0; t<ExpBasis.size(); t++) {
                    Exponents[t] = Exp1[t]*Exp2[t][i2]*Exp3[t][i3];
                }
                double res = getDensityValue(x,y,z, Exponents);
                size_t j2 = (i2-(i2%C2))/C2; // Number of contracted cube point which values are currently being calculated
                size_t j3 = (i3-(i3%C3))/C3;
                Cache[j2][j3] += res;
                //------------------------------------------------------
                classic<<setw(16)<<std::scientific<<setprecision(8)<<res;
                if (i3 % 6 == 5) classic<<endl;
            }//End of outer z loop
            classic<<endl;
        }//End of outer y loop
        if(i1%C1 == C1-1) {
            cout << "Printing stored values to the contracted cube\n";
            for(size_t p2=0; p2<CtrDim2; p2++) {
            for(size_t p3=0; p3<CtrDim3; p3++) {
                contracted<<setw(16)<<std::scientific<<setprecision(8)<<Cache[p2][p3];
                Cache[p2][p3] = 0;
                if (p3 % 6 == 5) contracted<<endl;
            }
            contracted << endl;
            }
        }
    }//End of outer x loop
    contracted.close();
    classic.close();
    cout << "ExpTime = " << ExpTIme/CLOCKS_PER_SEC << endl;
    cout << "mulTime = " << MulTime/CLOCKS_PER_SEC << endl;
    cout << "xyzTime = " << XyzTime/CLOCKS_PER_SEC << endl;
}
