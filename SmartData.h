#ifndef SMARTDATA_H_INCLUDED
#define SMARTDATA_H_INCLUDED

#include"AbInitData.cpp"
#include"PackedBasis.cpp"
#include"PrintableVec3d.cpp"

class SmartOrbital{ // Only s,p,d,f,g functions
public:
	SmartOrbital() : xyzBasis(0){xyzBasis.clear();}
	bool setDeCartFromFF(const AbInitData& ini, const size_t OrbN, double** const PreCalcXYZ);

	vector<PackedXyzBasis> xyzBasis;
};

class SmartData {
    SmartData(const SmartData& B)=delete;
    SmartData& operator=(const SmartData& B)=delete;

    class AlphaBasis;
    double** PreCalcXYZ;
public:
    vector<GeomAtom> geom;
    vector<AlphaBasis> ExpBasis;
    vector<SmartOrbital> NewOrbitals;

    SmartData(const AbInitData& ini);
   ~SmartData(){
        for(size_t a=0; a<geom.size(); a++) {
            delete[] PreCalcXYZ[a];
        }
        delete[] PreCalcXYZ;
    }
    double getDensityValue(const double& x, const double& y, const double& z) const;
    double getDensityValue(const double& x, const double& y, const double& z, double* Exp) const;

    void PrintBothArbitraryCubes(const string& comment,
               const double& xOrig, const double& yOrig, const double& zOrig,
               const Vec3D& e1, const Vec3D& e2, const Vec3D& e3,
               const size_t& Dim1, const size_t& Dim2, const size_t& Dim3,
               const size_t& CtrDim1, const size_t& CtrDim2, const size_t& CtrDim3,
               const char* f_uncontr_name, const char* f_contr_name)
               const;
    void PrintBothOrthogonalCubes(const string& comment,
               const double& xOrig, const double& yOrig, const double& zOrig,
               const Vec3D& e1, const Vec3D& e2, const Vec3D& e3,
               const size_t& Dim1, const size_t& Dim2, const size_t& Dim3,
               const size_t& CtrDim1, const size_t& CtrDim2, const size_t& CtrDim3,
               const char* f_uncontr_name, const char* f_contr_name)
               const;
private:
    SmartData()=delete;
};

class SmartData::AlphaBasis {
    AlphaBasis(const AlphaBasis& B)=delete;
    AlphaBasis& operator=(const AlphaBasis& B)=delete;
public:
    double x,y,z,alpha;
//    inline double getC2(const double& sUnit) {return exp(-alpha*2*sUnit*sUnit);}
//
//    inline double calcC1X(const double& xUnit, const double& xOrig) {return exp(-alpha*(2*(xOrig-x)*xUnit - xUnit*xUnit));}
//    inline double calcC1Y(const double& yUnit, const double& yOrig) {return exp(-alpha*(2*(yOrig-y)*yUnit - yUnit*yUnit));}
//    inline double calcC1Z(const double& zUnit, const double& zOrig) {return exp(-alpha*(2*(zOrig-z)*zUnit - zUnit*zUnit));}
    inline double getValue(const double& valueX, const double& valueY, const double& valueZ) const {
        const double dx = valueX - x;
        const double dy = valueY - y;
        const double dz = valueZ - z;

        const double r2 = dx*dx + dy*dy + dz*dz;
        return exp(-alpha*r2);
    }
    inline double getValueOrt(const size_t n, const Vec3D& vec, const Vec3D& Orig) const {
        Vec3D r0(Orig.x-x, Orig.y-y, Orig.z-z);
        double rc = r0*vec;
        return exp(-alpha*(rc*rc/vec.slen + 2*n*rc + n*n*vec.slen));
    }

    inline double getValueOrtX(const size_t n, const Vec3D& vec, const Vec3D& Orig) const {
        Vec3D r0(Orig.x-x, Orig.y-y, Orig.z-z);
        return exp(-alpha*(r0.x*r0.x + 2*n*(r0*vec) + n*n*vec.slen));
    }
    inline double getValueOrtY(const size_t n, const Vec3D& vec, const Vec3D& Orig) const {
        Vec3D r0(Orig.x-x, Orig.y-y, Orig.z-z);
        return exp(-alpha*(r0.y*r0.y + 2*n*(r0*vec) + n*n*vec.slen));
    }
    inline double getValueOrtZ(const size_t n, const Vec3D& vec, const Vec3D& Orig) const {
        Vec3D r0(Orig.x-x, Orig.y-y, Orig.z-z);
        return exp(-alpha*(r0.z*r0.z + 2*n*(r0*vec) + n*n*vec.slen));
    }

    AlphaBasis():x(0),y(0),z(0),alpha(0) {}
    AlphaBasis(AlphaBasis&& B) : x(B.x),y(B.y),z(B.z),alpha(B.alpha) {}
    AlphaBasis(const PackedXyzBasis& B):x(0),y(0),z(0),alpha(0){B.getXyzAlpha(x,y,z,alpha);}
};

#endif // SMARTDATA_H_INCLUDED
