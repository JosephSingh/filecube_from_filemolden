#include<iostream>
#include<cmath>

#define PI 3.14159265359

double doubleFactorial (const int& i){
	if (i<-1) std::cout<<"WARNING! DOUBLE FACTORIAL FOR VALUE "<<i<<" ARE WRONG CALCULATED\n";
	double res(1);
	for (int j=i; j>1; j-=2) res*=j;
	return res;
}

double binomialCoefficient (const int& n, const int& i){
	if ((n>20)||(n<0)||(i<0)||(i>n)) std::cout<<"WARNING! BINOMIAL COEFFICIENT MAY BE CALCULATED WITH ERROR\n";
	double res(1);
	for (int j=1; j<=i; j++){ res*=(n-j+1); res/=j;}
	return res;
}

double overlap_I1_1D(const double& x1, const double& alpha1, const int& n1,
                     const double& x2, const double& alpha2, const int& n2){
	const double gamma = alpha1 + alpha2;
	const double P = ( alpha1 * x1 + alpha2 * x2 ) / gamma ;
	double res=0;
	for (int i1=0; i1<=n1; i1++)
		for (int i2=0; i2<=n2; i2++)
			if ((i1+i2)%2==0)
				res+=binomialCoefficient(n1,i1)*binomialCoefficient(n2,i2)*doubleFactorial(i1+i2-1)*
					pow(P-x1,n1-i1)*pow(P-x2,n2-i2)/pow(2.0*gamma,(i1+i2)/2);
	res *= sqrt(PI/gamma)*exp(-alpha1*alpha2/gamma*(x1-x2)*(x1-x2));
	return res;
}

double overlap_I1_3D(const double& x1, const double& y1, const double& z1, const double& alpha1,
                     const int& nx1, const int& ny1, const int& nz1,
                     const double& x2, const double& y2, const double& z2, const double& alpha2,
                     const int& nx2, const int& ny2, const int& nz2){
	double res = overlap_I1_1D (x1, alpha1, nx1, x2, alpha2, nx2);
	res *= overlap_I1_1D (y1, alpha1, ny1, y2, alpha2, ny2);
	res *= overlap_I1_1D (z1, alpha1, nz1, z2, alpha2, nz2);
	return res;
}
