#include"SmartData.cpp"

int main(int argc, char *argv[]) {
    /// 1.Input
    string filename = "";            //Names of *.cube and *contracted.cube files
    string filename2= "";            //
    double Orig[3] = {0.1,0.1,0.1};  //Cube origin point
    Vec3D e1(0.1,0.0,0.0);           //
    Vec3D e2(0.0,0.1,0.0);           //
    Vec3D e3(0.0,0.0,0.1);           //
    size_t Grid[3] = {256, 256, 256};//Cube dimensions
    size_t Cntr[3] = {64, 64, 64};   //Contracted cube dimensions

    if(argc == 1) {
        cout << "Testing mode: LiH, CH4, 64^3 and 16^3 aligned by cartesian axes\n";
        filename = "ch4_avqz.molden";
        Grid[0] = 64;
        Grid[1] = 64;
        Grid[2] = 64;
        Cntr[0] = Grid[0]/4;
        Cntr[1] = Grid[1]/4;
        Cntr[2] = Grid[2]/4;
    }
    else if(argc == 2) {
        cout << "Everything is default. 256^3 cube and 64^3, both aligned by cartesian axes\n";
        filename = argv[1];
    }
    else if(argc == 5) {
        filename = argv[1];
        cout << "Making cubes of different size, X^3 and X/4^3 aligned by cartesian axes\n";
        Grid[0] = stoi(argv[2]);
        Grid[1] = stoi(argv[3]);
        Grid[2] = stoi(argv[4]);
        Cntr[0] = Grid[0]/4;
        Cntr[1] = Grid[1]/4;
        Cntr[2] = Grid[2]/4;
    }
    else if(argc == 8) {
        cout << "Everything is user-defined\n";
        filename = argv[1];
        ifstream vecs("CubeAlignment.dat");
        if(vecs.good()) {
            cout << "Vectors e1, e2, e3 are imported from " << "CubeAlignment.dat" << endl;
            vecs>>e1>>e2>>e3;
        }
        else {
            cout << "File CubeAlignment.dat cannot be read. Check its existence or run program with default parameters\n";
            return 1;
        }
        Grid[0] = stoi(argv[2]);
        Grid[1] = stoi(argv[3]);
        Grid[2] = stoi(argv[4]);
        cout << "Cube parameters: " << Grid[0]<<" "<<Grid[1]<<" "<<Grid[2]<<endl;
        Cntr[0] = stoi(argv[5]);
        Cntr[1] = stoi(argv[6]);
        Cntr[2] = stoi(argv[7]);
        cout << "Contracted cube: " << Cntr[0]<<" "<<Cntr[1]<<" "<<Cntr[2]<<endl;
    }
    else {
        cout << "You shold give either 1 or 4 or 7 parameters to the program. Look README.txt for additional info.\n";
        return 1;
    }

    /// 2.Reading basis, orbitals and geometry
    AbInitData A(filename.c_str());
    double xc, yc, zc;
    /// 3.Calculating mass center and moving cube origin according to the result
    if(A.calcMassCenter(xc,yc,zc, "atomic_mass.dat")) {
        Orig[0] = xc - (e1.x*Grid[0] + e2.x*Grid[1] + e3.x*Grid[2])/2;
        Orig[1] = yc - (e1.y*Grid[0] + e2.y*Grid[1] + e3.y*Grid[2])/2;
        Orig[2] = zc - (e1.z*Grid[0] + e2.z*Grid[1] + e3.z*Grid[2])/2;
        cout << "   Cube center is now in the same place as mass center\n\n";
    }
    else {
        cout << "   Unable to calculate mass center. Cube origin is (x,y,z) " << Orig[0] <<" "<< Orig[1] <<" "<< Orig[2] <<endl;
    }
    /// 4.Rearranging orbitals to sum(by all different exponents){(c1*x+c2*y+c3*z+c4*x^2+...)*exp(-ar2)} for faster calculations
    SmartData B(A);// c[i] are neither orbital coefficints, nor coefficints from basis functions. They are combination of both
    /// 5.Making names for output files
    size_t p = filename.find_first_of('.');
    filename = filename.substr(0,p) + "_" + to_string(Grid[0]) + "x" + to_string(Grid[1]) + "x" + to_string(Grid[2]) + ".cube";
    filename2= filename.substr(0,p) + "_" + to_string(Cntr[0]) + "x" + to_string(Cntr[1]) + "x" + to_string(Cntr[2]) + "_contracted.cube";
    /// 6.Calculating cube and measuring time
    Timer T;
    try {
        if(e1*e2 || e2*e3 || e1*e3) {
            B.PrintBothOrthogonalCubes("***", Orig[0],Orig[1],Orig[2], e1,e2,e3, Grid[0],Grid[1],Grid[2], Cntr[0],Cntr[1],Cntr[2], filename.c_str(), filename2.c_str());
        }
        else {
            B.PrintBothOrthogonalCubes("***", Orig[0],Orig[1],Orig[2], e1,e2,e3, Grid[0],Grid[1],Grid[2], Cntr[0],Cntr[1],Cntr[2], filename.c_str(), filename2.c_str());
        }
    }
    catch(string s) {
        cout << s;
        return 1;
    }
    T.print();
    //T.restart();
    //B.PrintBothCartesianAlignedCube("***",Orig[0],Orig[1],Orig[2], e1.x,e2.y,e3.z, Grid[0],Grid[1],Grid[2], Cntr[0],Cntr[1],Cntr[2], filename.c_str(), filename2.c_str());
    //T.print();
    return 0;
}
